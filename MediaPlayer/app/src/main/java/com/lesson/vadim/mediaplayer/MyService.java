package com.lesson.vadim.mediaplayer;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


public class MyService extends Service{
    private static final String TAG = "myLogs";

    Player p = new Player();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw  new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "Create");
        p.mediaPlayer.setLooping(false);

    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d(TAG, "Start");
        p.mediaPlayer.start();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Stop");
        p.mediaPlayer.stop();
    }
}
