package com.lesson.vadim.mediaplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class Player extends AppCompatActivity implements View.OnClickListener{

    static MediaPlayer mediaPlayer;
    ArrayList<File> mySongs;
    int position;
    SeekBar seekBar;
    private Button btnPlay, btnFF, btnFB, btnNext, btnPrev;
    private ImageButton btnListSongs;
    private TextView tvTitle, timeStart;
    Uri uri;
    Thread updateSeekBar;
    private String songTitle;
    int totalDuration;
    int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnFF = (Button) findViewById(R.id.btnFF);
        btnFB = (Button) findViewById(R.id.btnFW);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnPrev = (Button) findViewById(R.id.btnPrev);
        btnListSongs = (ImageButton) findViewById(R.id.btnListSongs);
        btnPlay.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnFB.setOnClickListener(this);
        btnFF.setOnClickListener(this);
        btnListSongs.setOnClickListener(this);

        tvTitle = (TextView)findViewById(R.id.tvTitle);
        timeStart = (TextView)findViewById(R.id.songCurrent);


        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();

        }

        startService();
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        updateSeekBar = new Thread() {
            @Override
            public void run() {
                totalDuration = mediaPlayer.getDuration();
                currentPosition = 0;
                seekBar.setMax(totalDuration);
                if(currentPosition == totalDuration){
                    Toast.makeText(Player.this, "in", Toast.LENGTH_SHORT).show();
                }

                while (currentPosition < totalDuration) {
                    try {
                        sleep(500);
                        currentPosition = mediaPlayer.getCurrentPosition();
                        seekBar.setProgress(currentPosition);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        };

        Intent i = getIntent();
        Bundle bundle = i.getExtras();

        mySongs = (ArrayList) bundle.getParcelableArrayList("song");
        position = bundle.getInt("position", 0);

        initNameSong();

        uri = Uri.parse(mySongs.get(position).toString());
        mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);


        mediaPlayer.start();
        seekBar.setMax(mediaPlayer.getDuration());
        updateSeekBar.start();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                timeStart.setText(getTimeString(currentPosition));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnPlay:
                if (mediaPlayer.isPlaying()) {
                    btnPlay.setText(">");
                    mediaPlayer.pause();
                }else {
                    btnPlay.setText("||");
                    mediaPlayer.start();
                }
                break;
            case R.id.btnFF:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);
                break;
            case R.id.btnFW:
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);
                break;
            case R.id.btnNext:
                mediaPlayer.stop();
                mediaPlayer.reset();
                position = (position + 1) % mySongs.size();
                uri = Uri.parse(mySongs.get(position).toString());
                initNameSong();
                mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                seekBar.setMax(mediaPlayer.getDuration());
                mediaPlayer.start();
                break;
            case R.id.btnPrev:
                mediaPlayer.stop();
                mediaPlayer.reset();
                position = (position - 1 < 0) ? mySongs.size() - 1 : position - 1;
                uri = Uri.parse(mySongs.get(position).toString());
                initNameSong();
                mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
                seekBar.setMax(mediaPlayer.getDuration());
                mediaPlayer.start();
                break;
            case R.id.btnListSongs:
                mediaPlayer.stop();
                mediaPlayer.reset();
                Intent intent = new Intent(Player.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000*60*60));
        int minutes = (int) (( millis % (1000*60*60) ) / (1000*60));
        int seconds = (int) (( ( millis % (1000*60*60) ) % (1000*60) ) / 1000);
        buf
                .append(String.format("%02d", hours)).append(":")
                .append(String.format("%02d", minutes)).append(":")
                .append(String.format("%02d", seconds));
        return buf.toString();
    }

    public void initNameSong(){
        songTitle = mySongs.get(position).getName();
        tvTitle.setText(songTitle);
    }

    public void startService() {
        startService(new Intent(this, MyService.class));
    }

    public void stopService() {
        stopService(new Intent(this, MyService.class));
    }

}

