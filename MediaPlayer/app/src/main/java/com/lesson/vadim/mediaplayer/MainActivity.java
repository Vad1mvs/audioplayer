package com.lesson.vadim.mediaplayer;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView list;
    String[] items;

  public final ArrayList<File> mySongs = findSong(Environment.getExternalStorageDirectory());
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        list = (ListView) findViewById(R.id.list);

        list.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.song_layout, R.id.songTitle, new ArrayList<String>()));
        new MyTask().execute();
        adapter = (ArrayAdapter) list.getAdapter();
        adapter.notifyDataSetChanged();


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Player.class);
                intent.putExtra("position", position);
                intent.putExtra("song", mySongs);
                startActivity(intent);
            }
        });

    }

    public ArrayList<File> findSong(File root) {

        ArrayList<File> arrayList = new ArrayList<File>();
        File[] files = root.listFiles();

        for (File singleFile : files) {
            if (singleFile.isDirectory() && !singleFile.isHidden()) {
                arrayList.addAll(findSong(singleFile));
            } else {
                if (singleFile.getName().endsWith(".mp3") || singleFile.getName().endsWith(".MP3")) {
                    arrayList.add(singleFile);
                }
            }
        }
        return arrayList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) menu.findItem(R.id.menu_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        android.support.v7.widget.SearchView.OnQueryTextListener queryTextListener = new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MainActivity.this.adapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                MainActivity.this.adapter.getFilter().filter(newText);
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    class MyTask extends AsyncTask<Void, String, Void>{
        ArrayAdapter<String> adapter;

        @Override
        protected void onPreExecute() {
            adapter = (ArrayAdapter<String>) list.getAdapter();
        }

        @Override
        protected Void doInBackground(Void... voids) {

                items = new String[mySongs.size()];
                for (int i = 0; i < mySongs.size(); i++) {

                    items[i] = mySongs.get(i).getName().toString().replace(".mp3", "");

                }
                publishProgress(items);
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for(int i =0; i<values.length;i++){
                adapter.add(values[i]);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
        }
    }

}
